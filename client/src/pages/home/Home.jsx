import { Fragment, useEffect, useState } from 'react'
import Header from '../../components/header/Header'
import './home.scss'
import Posts from '../../components/posts/Posts'
import SideBar from '../../components/sideBar/SideBar'
import axios from 'axios'
import { useLocation } from 'react-router-dom'

function Home() {
  const [posts, setPosts] = useState([])
  const { search } = useLocation()

  useEffect(() => {
    const fetchPosts = async () => {
      const res = await axios.get('/posts' + search)
      setPosts(res.data)
    }
    fetchPosts()
  }, [search])
  return (
    <Fragment>
      <Header />
      <div className='home'>
        <Posts posts={posts} />
        <SideBar />
      </div>
    </Fragment>
  )
}

export default Home
