import { useState } from 'react'
import './register.css'
import { Link } from 'react-router-dom'

import axios from 'axios'

function Register() {
  const [username, setUserName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault()

    try {
      setError(false)
      const res = await axios.post('/auth/register', {
        username,
        email,
        password,
      })
      res.data && window.location.replace('/login')
    } catch (err) {
      setError(true)
    }
  }

  return (
    <div className='register'>
      <span className='registerTitle'>Register</span>
      <form className='registerForm' onSubmit={handleSubmit}>
        <label>Username</label>
        <input
          className='registerInput'
          type='text'
          placeholder='Enter your Username.....'
          onChange={(e) => setUserName(e.target.value)}
        />
        <label>Email</label>
        <input
          className='registerInput'
          type='text'
          placeholder='Enter your Email.....'
          onChange={(e) => setEmail(e.target.value)}
        />
        <label>Password</label>
        <input
          className='registerInput'
          type='text'
          placeholder='Enter your Password.....'
          onChange={(e) => setPassword(e.target.value)}
        />
        <button className='registerButton' type='submit'>
          Register
        </button>
      </form>
      <button className='registerLoginButton'>
        <Link to='/login' className='link'>
          LogIn
        </Link>
      </button>
      {error && (
        <span style={{ color: 'red', marginTop: '10px' }}>
          something went wrong!{' '}
        </span>
      )}
    </div>
  )
}

export default Register
