import { useContext, useRef } from 'react'
import './login.css'
import { Link } from 'react-router-dom'
import { Context } from '../../context/Context'
import axios from 'axios'

function Login() {
  const userRef = useRef()
  const passswordRef = useRef()
  const { dispatch, isFetching } = useContext(Context)
  const handleSubmit = async (e) => {
    e.preventDefault()
    dispatch({ type: 'LOGIN_START' })
    try {
      const res = await axios.post('/auth/login', {
        username: userRef.current.value,
        password: passswordRef.current.value,
      })
      dispatch({ type: 'LOGIN_SUCCESS', payload: res.data })
    } catch (err) {
      dispatch({ type: 'LOGIN_FAILYRE' })
    }
  }

  return (
    <div className='login'>
      <span className='loginTitle'>Login</span>
      <form className='loginForm' onSubmit={handleSubmit}>
        <label>UserName</label>
        <input
          className='loginInput'
          ref={userRef}
          type='text'
          placeholder='Enter your UserName.....'
        />
        <label>Password</label>
        <input
          className='loginInput'
          type='text'
          placeholder='Enter your Password.....'
          ref={passswordRef}
        />
        <button className='loginButton' type='submit' disabled={isFetching}>
          Login
        </button>
      </form>
      <button className='loginRegisterButton'>
        <Link to='/register' className='link'>
          Register
        </Link>
      </button>
    </div>
  )
}

export default Login
