import { useLocation, Link } from 'react-router-dom'
import './singlePost.scss'
import { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import { Context } from '../../context/Context'

function SinglePost() {
  const PF = 'http://localhost:5000/images/'
  const location = useLocation()
  const path = location.pathname.split('/')[2]
  const [post, setPost] = useState({})
  const { user } = useContext(Context)
  const [title, setTitle] = useState('')
  const [desc, setDesc] = useState('')
  const [updatMode, setupdatMode] = useState(false)

  useEffect(() => {
    const getPost = async () => {
      const res = await axios.get('/posts/' + path)
      setPost(res.data)
      setTitle(res.data.title)
      setDesc(res.data.desc)
    }
    getPost()
  }, [path])
  const handleDelet = async (e) => {
    e.preventDefault()
    try {
      await axios.delete('/posts/' + path, {
        data: { username: user.username },
      })
      window.location.replace('/')
    } catch (err) {}
  }
  const handleUpdate = async (e) => {
    e.preventDefault()
    try {
      await axios.put(`/posts/${post._id}`, {
        username: user.username,
        title,
        desc,
      })
      setupdatMode(false)
      //window.location.reload()
    } catch (err) {}
  }
  return (
    <div className='singlePost'>
      <div className='singlePostWrappr'>
        {post.photo && (
          <img src={PF + post.photo} alt='' className='singlePostImg' />
        )}
        {updatMode ? (
          <input
            type='text'
            autoFocus
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            className='singlePostTitleInput'
          ></input>
        ) : (
          <h1 className='singlePostTitle'>
            {title}
            {post.username === user.username && (
              <div className='singlePostEdit'>
                <i
                  className='singlePostIcon fa-regular fa-pen-to-square'
                  onClick={() => setupdatMode(true)}
                ></i>
                <i
                  className='singlePostIcon fa-regular fa-trash-can'
                  onClick={handleDelet}
                ></i>
              </div>
            )}
          </h1>
        )}

        <div className='singlePostInfo'>
          <span className='singlePostAuthor'>
            Author:
            <Link className='link' to={`/?user=${post.username}`}>
              <b>{post.username}</b>
            </Link>
          </span>
          <span className='singlePostDate'>
            {new Date(post.createdAt).toDateString()}
          </span>
        </div>
        {updatMode ? (
          <textarea
            className='singlePostDescInput'
            value={desc}
            onChange={(e) => setDesc(e.target.value)}
            autoFocus
          ></textarea>
        ) : (
          <p className='singlePostDesc'>{desc}</p>
        )}
        {updatMode && (
          <button className='singlePostButton' onClick={handleUpdate}>
            Update
          </button>
        )}
      </div>
    </div>
  )
}

export default SinglePost
