import { useState } from 'react'
import './sideBar.scss'
import { useEffect } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

function SideBar() {
  const [cat, setCat] = useState([])
  useEffect(() => {
    const getCats = async () => {
      const res = await axios.get('/categories')
      setCat(res.data)
    }
    getCats()
  }, [])
  return (
    <div className='sideBar'>
      <div className='sideBarItem'>
        <span className='sideBarTitle'>ABOUT ME</span>
        <img src='/assets/12.png' alt='' />
        <p>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam sunt
          vel dolor velit perferendis eum nostrum aspernatur ea similique atque.
        </p>
      </div>
      <div className='sideBarItem'>
        <span className='sideBarTitle'>CATEGORIES</span>
        <ul className='sideBarList'>
          {cat.map((c) => (
            <Link key={c._id} className='link' to={`/?cat=${c.name}`}>
              <li className='sideBarListItem'>{c.name}</li>
            </Link>
          ))}
        </ul>
      </div>
      <div className='sideBarItem'>
        <span className='sideBarTitle'>FOLLOW US</span>
        <div className='sideBarSocial'>
          <i className='sideBarIcon fa-brands fa-facebook'></i>
          <i className='sideBarIcon fa-brands fa-twitter'></i>
          <i className='sideBarIcon fa-brands fa-square-pinterest'></i>
          <i className='sideBarIcon fa-brands fa-instagram'></i>
        </div>
      </div>
    </div>
  )
}

export default SideBar
