export const LoginStart = (userCredanials) => ({
  type: 'LOGIN_START',
})

export const LoginSuccess = (user) => ({
  type: 'LOGIN_SUCCESS',
  payload: user,
})

export const LoginFailure = () => ({
  type: 'LOGIN_FAILYRE',
})
export const LogOut = () => ({
  type: 'LOGOUT',
})

export const UpdateStart = (userCredanials) => ({
  type: 'UPDATE_START',
})

export const UpdateSuccess = (user) => ({
  type: 'UPDATE_SUCCESS',
  payload: user,
})

export const UpdateFailure = () => ({
  type: 'UPDATE_FAILYRE',
})
