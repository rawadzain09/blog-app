import { useContext } from 'react'
import TopBar from './components/topBar/TopBar'
import Write from './components/write/Write'
import Home from './pages/home/Home'
import Login from './pages/login/Login'
import Register from './pages/register/Register'
import Settings from './pages/settings/Settings'
import Single from './pages/single/Single'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Context } from './context/Context'
function App() {
  const { user } = useContext(Context) 

  ///TODO:hello
  return (
    <BrowserRouter>
      <TopBar />
      <Routes>
      ///TODO:hello
        <Route path='/' element={<Home />}></Route>
        <Route
          path='/register'
          element={user ? <Home /> : <Register />}
        ></Route>
        ///TODO:hello
        <Route path='/login' element={user ? <Home /> : <Login />}></Route>
        <Route
          path='/settings'
          ///TODO:hello
          element={user ? <Settings /> : <Register />}
        ></Route>
        ///TODO:hello
        <Route path='/post/:postId' element={<Single />}></Route>
        ///TODO:hello
        <Route path='/write' element={user ? <Write /> : <Register />}></Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App
